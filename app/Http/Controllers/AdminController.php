<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Input;
use Validator;


class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo()
    {
    	return '/admin/home';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout','index','showLoginForm','adminLogin','showUserList', 'userBlock', 'userDelete');
        $this->middleware('admin')->except('showLoginForm','adminLogin');
    }

    public function index()
    {
        return view('admin-home');
    }

    public function showLoginForm()
    {
    	return view('admin-loginform');
    }

    public function adminLogin(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        //dd($valid->fails());
        if(!$valid->fails())
        {
            $credentials = $request->only('email', 'password', 'is_admin');
            if(Auth::attempt($credentials))
            {
                $admin = Auth::user();
                // dd($admin);
                $admin->last_login = Carbon::now();
                $admin->save();
                return redirect()->route('admin.home');
            }
            else
            {
                $errors = new MessageBag(['email' => ['These credentials do not match our records.']]);
                return redirect()->route('admin.loginform')->withErrors($errors)->withInput(Input::except('email'));
            }
        }
        else
        {
            return redirect()->route('admin.loginform')->withErrors($valid->errors());
        }
    }

    public function showUserList()
    {
        // $users = DB::table('users')->where(array(['is_admin', '0'], ['is_blocked', '0']))->get();
        $users = User::where(array(['is_admin', '0'], ['is_blocked', '0']))->get();
        return view('admin-userlist')->with('users', $users);
    }

    public function userBlock($id)
    {
        $user = DB::table('users')->where('id', $id)->update(['is_Blocked' => '1']);
        Session::flash('status', 'User has been Blocked');
        // $user->save();
        // $users = DB::table('users')->where(array(['is_admin', '0'], ['is_blocked', '0']))->get();
        // return view('admin-userlist')->with('users', $users);
        return redirect()->back();
    }

    public function userDelete($id)
    {
        // $user = DB::table('users')->where('id', $id)->update(['deleted_at', Carbon::now()]);
        $user = User::find($id);
        $user->delete();
        Session::flash('status', 'User has been Trashed');
        // $user->save();
        // $users = DB::table('users')->where(array(['is_admin', '0'], ['is_blocked', '0']))->get();
        // return view('admin-userlist')->with('users', $users);
        return redirect()->back();
    }

    public function userTrashed()
    {
        $users = User::onlyTrashed()->get();
        return view('admin-usertrashed')->with('users', $users);
    }

    public function userKill($id)
    {
        // $user = User::find($id);         //GIVES NULL WHEN RETRIEVING TRASHED DATA USING FIND METHOD.
        $user = User::withTrashed()->where('id', $id)->first();
        // dd($user);
        $user->forceDelete();
        // $user->destroy($id);
        Session::flash('status', 'User has been Permanently Deleted');
        // return view('admin-usertrashed')->with('users', $users);
        return redirect()->back();
    }

    public function userRestore($id)
    {
        // $user = User::find($id);         //GIVES NULL WHEN RETRIEVING TRASHED DATA USING FIND METHOD.
        $user = User::withTrashed()->where('id', $id)->first();
        // dd($user);
        $user->restore();
        // $user->destroy($id);
        Session::flash('status', 'User has been Restored');
        // return view('admin-usertrashed')->with('users', $users);
        return redirect()->back();
    }

    public function userBlocked()
    {
        $users = User::where('is_blocked','1')->get();
        // dd($user);
        return view('admin-userblocked')->with('users', $users);
    }

    public function userUnblock($id)
    {
        $user = User::where('id', $id);
        $user->update(['is_blocked' => '0']);
        Session::flash('status', 'User has been Unblocked');
        return redirect()->back();
        // return view('admin-userblocked')->with('users', $users);
    }

}
