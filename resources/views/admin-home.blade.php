@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><center>ADMIN Dashboard</center></div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <center>You are logged in as <strong>ADMIN</strong>!</center>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                	<a href="{{ route('admin.userlist') }}"><center><strong>USERS LIST</strong></center></a>
                </div>

				<div class="panel-heading">
					<a href="{{ route('admin.usertrashed') }}"><center><strong>TRASHED USERS</strong></center></a>
				</div>

				<div class="panel-heading">
					<a href="{{ route('admin.userblocked') }}"><center><strong>BLOCKED USERS</strong></center></a>
				</div>
			</div>
        </div>
    </div>
</div>
@endsection
