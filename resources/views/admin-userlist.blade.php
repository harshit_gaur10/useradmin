@extends('layouts.app')

@section('content')
<div class="panel panel-default">
	<div class="panel-body">
		<table class="table table-hover">
		<thead>
			<th>
				Name 
			</th>
			<th>
				Email 
			</th>
			<th>
				Mobile Number
			</th>
			<th>
				Date of Birth
			</th>
			<th>
				Image
			</th>
			<th>
				Block
			</th>
			<th>
				Trash
			</th>
		</thead>

		<tbody>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->mobileNumber }}</td>
					<td>{{ $user->dob }}</td>
					<td><img src="{{ asset($user->image) }}" alt="Image" width=70 height = 40></td>
					<td>
					<a href="{{ route('admin.userblock', ['id' => $user->id ]) }}" class="btn btn-s btn-info">Block</a>
					</td>
					<td>
					<a href="{{ route('admin.userdelete', ['id' => $user->id ]) }}" class="btn btn-s btn-danger">Trash</a>
				</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	</div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                	<a href="{{ route('admin.usertrashed') }}"><center><strong>TRASHED USERS</strong></center></a>
                </div>
                <div class="panel-heading">
                	<a href="{{ route('admin.userblocked') }}"><center><strong>BLOCKED USER</strong></center></a>
                </div>
				<div class="panel-heading">
					<a href="{{ route('admin.home') }}"><center><strong>ADMIN HOME</strong></center></a>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

