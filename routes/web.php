<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () 
{
	Route::get('/loginform', 'AdminController@showLoginForm')->name('admin.loginform');
	Route::post('/login', 'AdminController@adminLogin')->name('admin.login');
	Route::get('/home', 'AdminController@index')->name('admin.home');

	Route::get('/userlist', 'AdminController@showUserList')->name('admin.userlist');

	Route::get('/userblock/{id}', 'AdminController@userBlock')->name('admin.userblock');
	Route::get('/userdelete/{id}', 'AdminController@userDelete')->name('admin.userdelete');
	Route::get('/usertrashed', 'AdminController@userTrashed')->name('admin.usertrashed');
	Route::get('/userkill/{id}', 'AdminController@userKill')->name('admin.userkill');
	Route::get('/userrestore/{id}', 'AdminController@userRestore')->name('admin.userrestore');
	Route::get('/userblocked', 'AdminController@userBlocked')->name('admin.userblocked');
	Route::get('/userunblock/{id}', 'AdminController@userUnblock')->name('admin.userunblock');
});